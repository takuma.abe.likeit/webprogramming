<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>
</head>

<body>
	<h1>ログイン画面</h1>
     
      <c:if test="${errMsg != null}" >
	    <div style="color:red" class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>


	<form class="form-signin" action="LoginServlet" method="post">
		<div class="form-group">
			<label for="exampleInputId">ログインID</label>
			<input type="text" name="loginId"
				class="form-control" id="exampleInputId1"
				aria-describedby="emailHelp">
			<p></p>
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">パスワード</label> <input
				type="password" name="password" class="form-control" id="exampleInputPassword1">
		</div>
		<p></p>

		<button type="submit" class="btn btn-primary">ログイン</button>
	</form>

</body>

</html>

