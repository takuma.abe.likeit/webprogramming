<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <p class="navbar-text navbar-right">ユーザー名さん<a href="LoginServlet" class="navbar-link">ログアウト</a></p>


	<meta charset="UTF-8">
	<title>新規登録</title>
	</head>
    <body>
    <h2>ユーザー新規登録</h2>


    <form action="UserEntryServlet" method="post">
  <div class="form-group">
    <label for="exampleInputId">ログインID</label>
    <input type="text"  name="loginId" class="form-control" id="exampleInputId1" aria-describedby="emailHelp">
    <p></p>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">パスワード</label>
    <input type="password" name="password" class="form-control" id="exampleInputPassword1">
  </div>

　<div class="form-group">
    <label for="exampleInputPassword2">パスワード(確認)</label>
    <input type="password" name="password" class="form-control" id="exampleInputPassword2">
  </div>

　<div class="form-group">
    <label for="exampleInputUserName">ユーザー名</label>
    <input type="text" name="username" class="form-control" id="exampleInputUserName">
  </div>

　<div class="form-group">
    <label for="exampleInputBirthday">生年月日</label>
    <input type="text" name="birthday" class="form-control" id="exampleInputBirthday">
  </div>
      <p></p>
  <button type="submit" value="登録">登録</button>

<div class="navbar-header">
          <a class="navbar-brand" href="UserListServlet">戻る</a>
        </div>
</form>


	</body>
</html>

