package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import model.User;

public class UserEntryDao {

	//データベースに登録
	public UserEntryDao(User u) {
		Connection con = null;
		try {
			con = DBManager.getConnection();
			String sql = "insert into user (loginId, name, birthDate, password) VALUES (?, ?, ?, ?)";
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setString(1, u.getLoginId());
			ps.setString(2, u.getName());
			ps.setDate(3, (Date) u.getBirthDate());
			ps.setString(4, u.getPassword());

			int r = ps.executeUpdate();//ここに来ない
			System.out.println("ここまできた");
			if (r != 0) {
				System.out.println("新規登録成功");
			} else {
				System.out.println("新規登録失敗");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
