package controller;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserEntryDao;
import model.User;

/**
 * Servlet implementation class UserEntryServlet
 */
@WebServlet("/UserEntryServlet")
public class UserEntryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserEntryServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userEntry.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String name = request.getParameter("username");
		String birthDate = request.getParameter("birthday");

		// userEntry.jspから受け取った値をビーンズにセット
		User u = new User();

		u.setLoginId(loginId);
		u.setPassword(password);
		u.setName(name);
		u.setBirthDate(Date.valueOf(birthDate));
		System.out.println(u.getName());
		// アカウントをDBに登録
		UserEntryDao ued = new UserEntryDao(u);//うまく登録できない
		//ユーザ一覧に表示、ここまでは行く
		System.out.println("リスト表示");
		
		// セッションにアカウント情報を保存
		HttpSession session = request.getSession();
		session.setAttribute("user", u);
		System.out.println("セッションおｋ");
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);

	}

}
